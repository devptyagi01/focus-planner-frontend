import React, { useState, useEffect } from 'react'
import './Dashboard.css';
import Navbar from './Navbar';
import axios from '../util/axios';
import Task from './Task';
import Timer from './Timer';

const Dashboard = () => {

    const [showAddTask, setShowAddTask] = useState(false);
    const [tasks, setTasks] = useState([]);
    const [date] = useState(new Date(new Date().setDate(new Date().getDate())).toString().split(' ').splice(0, 4).join(' '))
    const [showTimer, setShowTimer] = useState(false);
    const [taskInTimer, setTaskInTimer] = useState(undefined);

    useEffect(() => {
        getAllTasks();
    }, [])

    useEffect(() => {
        if(taskInTimer) {
            setShowTimer(true)
        } else {
            setShowTimer(false)
        }
    }, [taskInTimer])

    const toggleShowAddTask = () => {
        setShowAddTask(!showAddTask);
    }

    const endTimer = () => {
        setTaskInTimer(undefined);
        setShowTimer(false);
    }

    const runTaskInTimer = (task) => {
        console.log(task);
        setTaskInTimer(task);
    }

    const getAllTasks = () => {
        axios.get(`task/get`, { params: { loggedUser: localStorage.getItem('username') } }).then(res => {
			const usertasks = res.data;
			usertasks.reverse();
			setTasks(usertasks);
		});
    }

    const deleteTask = (id) => {
        const loggedUser = localStorage.getItem('username');
        axios
			.delete(`task/delete`, {
				data: { id },
				params: { loggedUser },
			})
			.then(() => getAllTasks());
    }

    const addTask = (e) => {
        if (e.key === 'Enter' && e.target.value !== '') {
			axios
				.post(
					`task/post`,
					{ title: e.target.value, date: date },
					{ params: { loggedUser: localStorage.getItem('username') } }
				)
				.then(() => {
					getAllTasks();
                    e.target.value = '';
				});
		}
    }

    return (
        <div className="dashboard">
            <Navbar className="dashboard__sidebar"/>
            <div className="dashboard__body">
                
                {
                    showTimer ? (
                        <Timer task={taskInTimer} onEnd={endTimer} getAllTasks={getAllTasks}/>
                    ) : (
                        <>
                        <h2 className="today_date">{date}</h2>
                            <div className="section__add">
                            {
                                showAddTask ? 
                                (
                                    <div className="addTask" onKeyDown={e => addTask(e)}>
                                        <input className="input_add_new" type="text" placeholder="Add new task"/>
                                    </div>
                                ) : null
                            }
                            </div>

                            <div className="taks_list">
                                {tasks.map(task => <Task key={task._id} task={task} onDeleteClicked={deleteTask} getAllTasks={getAllTasks} onStartClick={runTaskInTimer}/>)}
                            </div>

                            <div className={`btn__newTask ${!showAddTask ? 'btn_add' : 'btn_cancel'}`} onClick={toggleShowAddTask}>
                                {showAddTask ? 'CANCEL' : 'ADD NEW TASK'}
                            </div>
                                    </>
                                )
                            }
            </div>
        </div>
    )
}

export default Dashboard
