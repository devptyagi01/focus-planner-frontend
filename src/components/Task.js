import React, {useState} from 'react'
import './Task.css';
import Slider from '@material-ui/core/Slider';
import axios from '../util/axios';


const Task = ({task, onDeleteClicked, getAllTasks, onStartClick}) => {

    const [showAllOptions, setShowAllOptions] = useState(false);
    const [workTime, setWorkTime] = useState(task.workTime);
    const [breakTime, setBreakTime] = useState(task.breakTime);

    const handleWorkTimeChange = (event, newValue) => {
        setWorkTime(newValue);
    };

    const handleBreakTimeChange = (event, newValue) => {
        setBreakTime(newValue);
    };

    const toggleShowAllOptions = () => {
        setShowAllOptions(!showAllOptions);
    }

    const updateTask = () => {
        axios.put(
				`task/put`,
				{
					id: task._id,
					title: task.title,
					color: '',
					note: '',
					workTime: workTime,
					breakTime: breakTime,
				},
				{ params: { loggedUser: localStorage.getItem('username') } }
			)
			.then(() => {
				getAllTasks()
                toggleShowAllOptions()
			});
    }

    return (
        <div className="task_single">
            <div className="task__display">
                <div><h2>{task.title}</h2> ({task.workTime} minutes)</div>
                <div className="task_single_controls">
                    <i className="fas fa-hourglass-start" onClick={() => onStartClick(task)}></i>
                    <i className="fas fa-edit" onClick={() => toggleShowAllOptions()}></i>
                    <i className="fas fa-trash-alt" onClick={() => onDeleteClicked(task._id)}></i>
                </div>
            </div>
            {
                showAllOptions ? 
                (
                    <div className="allOptions">
                        <h3>Work Time (Mins)</h3>
                        <Slider 
                            value={workTime} 
                            onChange={handleWorkTimeChange} 
                            aria-labelledby="discrete-slider" 
                            valueLabelDisplay="auto"
                            step={5}
                            marks
                            min={10}
                            max={120}
                            />

                        <h3>Break Time (Mins)</h3>
                        <Slider 
                            value={breakTime} 
                            onChange={handleBreakTimeChange} 
                            aria-labelledby="discrete-slider" 
                            valueLabelDisplay="auto"
                            step={5}
                            marks
                            min={0}
                            max={60}
                            />
                        <div className="btn_save" onClick={updateTask}>
                            Save
                        </div>
                    </div>
                ) : null
            }
        </div>
    )
}

export default Task
