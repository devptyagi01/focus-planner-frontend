import React from 'react'
import './Timer.css';
import {CountdownCircleTimer} from 'react-countdown-circle-timer';
import { useState } from 'react';
import axios from '../util/axios';

const Timer = ({task, onEnd, getAllTasks}) => {

    const minuteSeconds = 60;
    const hourSeconds = 3600;
    const daySeconds = 86400;


    const getTimeSeconds = (time) => (minuteSeconds - time) | 0;
    const getTimeMinutes = (time) => ((time % hourSeconds) / minuteSeconds) | 0;
    const getTimeHours = (time) => ((time % daySeconds) / hourSeconds) | 0;
    const getTimeDays = (time) => (time / daySeconds) | 0;

    const [isWorkPlaying, setWorkPlaying] = useState(true);
    const [isBreakPlaying, setBreakPlaying] = useState(false);
    const [timerType, setTimerType] = useState('WORK');

    const [remainingWorkTime, setRemainingWorkTime] = useState(task.workTime);
    const [remainingBreakTime, setRemainingBreakTime] = useState(task.breakTime);

    const takeBreak = () => {
        setTimerType('BREAK');
        setBreakPlaying(true);
        setWorkPlaying(false);
    }

    const endSession = () => {
        setWorkPlaying(false);
        setBreakPlaying(false);
        onEnd();
        axios.put(
				`task/put`,
				{
					id: task._id,
					title: task.title,
					color: '',
					note: '',
					workTime: getTimeMinutes(remainingWorkTime),
					breakTime: getTimeMinutes(remainingBreakTime),
				},
				{ params: { loggedUser: localStorage.getItem('username') } }
			)
			.then(() => {
				getAllTasks();
			});
    }

    const getRemainingWorkMinutes = (remainingTime) => {
        setRemainingWorkTime(remainingTime);
        return getTimeMinutes(remainingTime)
    }

    const getRemainingBreakMinutes = (remainingTime) => {
        setRemainingBreakTime(remainingTime);
        return getTimeMinutes(remainingTime)
    }

    return (
        <div className="timer"> 
            <h1 className="taskTitle">{task.title}</h1>
            {timerType === 'WORK' ? (
                <>
                <h2>Work Mode</h2>
                <CountdownCircleTimer
                    isPlaying={isWorkPlaying}
                    duration={task.workTime * 60 }
                    colors={[
                    ['#004777', 0.33],
                    ['#F7B801', 0.33],
                    ['#A30000', 0.33],
                    ]}
                >
                    {({ remainingTime }) => getRemainingWorkMinutes(remainingTime) + 1}
                </CountdownCircleTimer>
                </>
            ): (
                <>
                <h2>Break Time</h2>     
                <CountdownCircleTimer
                    isPlaying={isBreakPlaying}
                    duration={task.breakTime * 60 }
                    colors={[
                    ['#004777', 0.33],
                    ['#F7B801', 0.33],
                    ['#A30000', 0.33],
                    ]}
                >
                    {({ remainingTime }) => getRemainingBreakMinutes(remainingTime) + 1}
                </CountdownCircleTimer>
                </>
            )}

            <div className="timer_controls">
                <div className="control take_break" onClick={() => takeBreak()}>
                    Take a Break
                </div>

                <div className="control end_session" onClick={() => endSession()}>
                    End Session
                </div>
            </div>
        </div>
    )
}

export default Timer
